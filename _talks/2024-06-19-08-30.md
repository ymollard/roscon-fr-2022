---
day: 3
title: Ouverture de la conférence ROSCON France 2024
authors:
  - name: Catherine Simon 
    affiliation: Secrétariat Général Pour l'Investissement
  - name: Ludovic Delval 
    affiliation: IRT Jules Verne
duration: 30
color: white
---
Ouverture de la conférence ROSCON France 2024
