---
day: 4
title: Conclusion de la conférence ROSCON France 2024
duration: 15
authors:
  - name: Ludovic Delval
    affiliation: IRT Jules Verne
  - name: Olivier Stasse
    affiliation: LAAS CNRS
color: white
---
Bilan de la conférence et présentation de la ROSCON France 2025 ?
