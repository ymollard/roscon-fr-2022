---
day: 4
title: Le challenge Aquabot - une competition à destination des étudiants
authors:
  - name: Guillaume Néron
    affiliation: SIREHNA
duration: 10
color: white
---
Sirehna, filliale à 100% de Naval Group, a organisé en 2023 une compétition de drone virtuelle à destination des étudiants ingénieurs. L'objectif de cette présentation est de présenter le déroulement de la compétition et de présenter quelques choix techniques qui ont permis sont bon déroulement. Un focus est également fait sur l'édition 2024