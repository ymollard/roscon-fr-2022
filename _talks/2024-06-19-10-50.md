---
day: 3
title: Tribulations d’un roboticien dans la robotique à impact - le cas de la robotique agricole
authors:
  - name: Mikael ARGUEDAS
    affiliation: NEOFARM
duration: 20
color: white
---
La robotique peut jouer un rôle significatif pour les enjeux sociaux et environnementaux des prochaines décennies. Nous présenterons ici un retour d’expérience sur le développement de plateformes robotiques sous contraintes (logistiques, pénuries de matériaux et composants, contraintes climatiques..) et comment l’open-source et la synergie entre low-tech et high-tech peut permettre de relever certains défis