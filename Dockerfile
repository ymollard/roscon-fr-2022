FROM ruby:latest

RUN gem install bundler jekyll

EXPOSE 4000
EXPOSE 35729
VOLUME /tmp/jekyll
WORKDIR /tmp/jekyll

CMD bundle exec jekyll serve --livereload -w --baseurl='' -d /tmp/_site
