© [Open Source Robotics Foundation](https://www.openrobotics.org/company), 2024
&mdash;
Conférence francophone 2024 organisée par [IRT Jules Verne](http://www.irt-jules-verne.fr)


Made with [Jekyll](https://jekyllrb.com/) using the [SinglePaged theme](https://github.com/t413/SinglePaged)
