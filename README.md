This is the source of the ROSCon Fr 2023 site.

It is forked from: https://github.com/t413/SinglePaged


== Testing ==

To test the site clone this repo and then run `./test_site.bash`

You will need docker installed.

And you can then find the site at http://localhost:4000
