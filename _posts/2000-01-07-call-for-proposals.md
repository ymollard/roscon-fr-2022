---
title: "CFP"
bg: "#7995ae"
color: "#FFFFFF"
fa-icon: bullhorn
style: left
---


## Appel à contributions

 * L'appel à contributions est désormais clôturé.

{% comment %}


## Format proposé

 * **Présentation classique** : 10, 20 ou 30 min de présentation + questions, durée à préciser lors de la soumission.
 * **Présentation éclair** : Format allégé du maximum de contraintes. Moins de 5 min, un slide, juste une vidéo ou même aucun support.
 * **Workshop** : Les workshops sont proposés sur trois formats :
     * Découverte - plutôt destiné aux profils débutants, jusqu'à 30 personnes et avec une durée conseillée de 90 minutes
     * Intermédiaire - Pour les profils plus expériementés, avec ou sans hardware, jusqu'à 15 personnes et une durée conseillée de 120 minutes
     * Expert - Pour les profils à l'aise sur ROS et souhaitant creuser un sujet particulier, avec ou sans hardware, jusqu'à 15 personnes et une durée conseillée de 120 minutes

Merci de préciser dans votre soumission si vous souhaitez ramener des éléments hardware.


Une proposition de présentation classique doit inclure :

 * Un titre
 * Un présentateur (nom et affiliation)
 * Une proposition de durée de l'intervention: Courte (~10 minutes), Moyenne (~20 minutes) ou Longue (~30 minutes)
 * Un résumé [maximum 100 mots]: utilisé pour introduire la présentation
 * Description détaillée [maximum 1000 mots]: Résumé, buts (que va apprendre l'audience ?), pointeurs vers les paquets qui vont être discutés.

Il est important d'inclure dans votre soumission suffisamment d'information pour que le comité de programme évalue l'importance et l'impact de votre présentation.
Nous encourageons vivement à fournir des liens sur des ressources disponibles publiquement. Ceci inclut les dépôts de code et les vidéos de démonstrations.


## Sujets

Tous les sujets liés à ROS sont les bienvenus, par exemple :

 * Bonnes pratiques
 * Nouveaux paquets
 * Développements spécifiques à un robot
 * Simulation de robot
 * Sûreté de fonctionnement et tolérance aux fautes
 * Systèmes embarqués
 * Développement de produit et commercialisation
 * Recherche et enseignement
 * Déploiement en entreprise
 * Organisation de communauté et direction
 * Test, qualité, et documentation
 * Compétitions robotiques et collaboration
 * Projets open source
 * ROS en production
 * ...

Nous demandons toutefois aux candidats d'éviter les conférences trop publicitaires sur l'activité de leur organisation ou entreprise.


Le formulaire de soumission est ici : <a href="https://2023.cfp.roscon.fr/cfp/">https://2023.cfp.roscon.fr/cfp/</a>.

Toutes les présentations liées à des sujets sur ROS 1 et 2 sont les bienvenues ! Par exemple présenter un paquet ROS ou une bibliothèque, explorer comment utiliser des outils, manipuler des données capteurs, et
des applications de robots, ou la façon dont vous utilisez ROS en développement ou en production. Les propositions seront évaluées par le comité scientifique qui effectuera la revue de votre proposition.

Nous souhaitons que ROSConFr représente la diversité de la communauté ROS. Nous encourageons les femmes, les membres de groupes minoritaires, et les membres de groupes sous représentés de participer à ROSConFr.

Nous vous invitons à discuter des idées que vous souhaiteriez partager sur le fil rosdiscourse de ROSConFr 2023 du <a href="https://discourse.ros.org/c/local/france">groupe ROS francophone</a>.



L'appel à soumissions est clos mais vous pouvez encore soumettre des [conférences éclairs](#présentations-éclairs) sur le <a href="https://2023.cfp.roscon.fr">CFP</a>.

Une proposition de présentation éclair doit seulement inclure une phrase décrivant son sujet.

## Sujets

Tous les sujets liés à ROS sont les bienvenus, par exemple :

 * Bonnes pratiques
 * Nouveaux paquets
 * Développements spécifiques à un robot
 * Simulation de robot
 * Sûreté de fonctionnement et tolérance aux fautes
 * Systèmes embarqués
 * Développement de produit et commercialisation
 * Recherche et enseignement
 * Déploiement en entreprise
 * Organisation de communauté et direction
 * Test, qualité, et documentation
 * Compétitions robotiques et collaboration
 * Projets open source
 * ROS en production
 * ...

Nous demandons toutefois aux candidats d'éviter les conférences trop publicitaires sur l'activité de leur organisation ou entreprise.

## Présentations éclairs

Vous n'avez pas eu le temps de rédiger une présentation, mais vous souhaiteriez communiquer sur votre dernier
projet, pas de souci il y a les présentations éclairs : un grand maximum de 5 minutes de temps de présentation avec un slide ou une vidéo ou simplement votre présence.

Cette année ces présentations pourront être soumises à l'avance dans le <a href="https://2023.cfp.roscon.fr">CFP</a>, mais comme pour les autres éditions des slots seront laissés ouverts jusqu'au matin de chaque jour de la conférence.

{% endcomment %}
