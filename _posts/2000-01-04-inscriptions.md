---
title: "Inscriptions"
bg: "#7995ae"
color: black
fa-icon: folder
style: center
---

La **<a href="{{ site.inscriptions_link }}">BILLETTERIE ROSConFr</a>** est ouverte avec paiement CB pour la **conférence** (190€ avant le {{ site.early_registration_deadline }} puis 260€ ensuite), le **soirée networking** (60€) et les options pour les visites du Manufacturing Lab et de l'IRT Jules Verne. Vous pouvez également commander votre T-Shirt et vous inscrire aux Workshops. Les conférenciers doivent également s'inscrire à la conférence. Les animateurs de Workshops n'ont pas besoin de s'inscrire aux workshops.

<script type="application/javascript"> function resizeIFrame(event) { if (event.data && event.data.height) { let iframe = document.querySelector('.website-section-iframe'); iframe.style.height = event.data.height; iframe.height = event.data.height; } } window.addEventListener('message', resizeIFrame, false); </script> <iframe scrolling="no" class="website-section-iframe" allow="geolocation" src="https://app.imagina.com/module/383138/111696?application_id=21005152" tabindex="-1" style="border: none; width: 100%; display: block; overflow: hidden;"> </iframe>


{% comment %}

La **<a href="{{ site.inscriptions_link }}">BILLETTERIE ROSConFr</a>** est ouverte avec paiement CB pour la **conférence** (190€ avant le {{ site.early_registration_deadline }} puis 260€ ensuite), le **banquet** (60€) et la visite de la **Cité du Vin** (22€). Le paiement par bon de commande n'est pas disponible. Les conférenciers doivent également s'inscrire.

⚠️ Les places assises étant limitées à 80 places et l'hébergement à Bordeaux étant déjà sous tension dû aux événements **RoboCup** et **Tour de France** concomittants, nous vous conseillons de réserver l'ensemble de votre voyage au plus tôt.

🗨️ N'hésitez pas à demander des conseils, répondre aux questions, ou discuter librement sur le salon **#rosconfr** du <a href="{{ site.discord_link }}">Discord RoboCup</a>.

Si vous souhaitez venir en famille, la visite des matchs de la RoboCup Major et Junior ainsi que certains événements joints sont ouverts à tous (cf les <a href="https://2023.robocup.org/visiteursinfos-pratiques/">informations grand public</a>). Les billets grand public ne donnent pas accès à la conférence.


Les orateurs ayant eu au moins une proposition retenue déposée sur le CFP doivent également s'inscrire à la conférence.

Il n'y a pas de tarifs réduits.
{% endcomment %}
