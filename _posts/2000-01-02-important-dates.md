---
title: "Dates"
bg: '#b7eff6'
color: "#545454"
style: left
fa-icon: calendar-days
---

{% include table_date.html %}

<br><br>
## Le déroulé de l' événement

<table style="width:100%;text-align:left;">
  <tr>
    <td>
      <p style="font-weight:bold;">Workshops </p>
      <p> Sur Inscription </p>
    </td>
    <td>
      <p>📆 17 juin 2024 après midi et 18 juin 2024 toute la journée</p>
      <p>🗺️ 17 juin - Ecole Centrale Nantes &ndash; <a href="https://maps.app.goo.gl/3o2eAHWA7icobVH1A"
          target="_blank">Google Maps</a></p>
      <p>🗺️ 18 juin - Bouguenais - IRT Jules Verne &ndash; <a href="https://maps.app.goo.gl/zbdrfiPeFbrEZo9X8"
          target="_blank">Google Maps</a>
      </p>
    </td>
  </tr>

  <tr>
    <td>
      <p style="font-weight:bold;">Conférence ROSConFr 🎊</p>
      <p>Présentiel au : </p>
    </td>
    <td>
      <p>📆 19 juin 2024 journée et 20 juin 2024 matin</p>
      <p>🗺️ Bouguenais - TechnoCampus Océan &ndash; <a href="https://maps.app.goo.gl/jEY6sHk8eHgVk3xMA">Google Maps</a>
      </p>
    </td>
  </tr>

  <tr>
    <td>
      <p style="font-weight:bold;">Soirée Networking</p>
    </td>
    <td>
      <p>📆 19 juin à 19:00</p>
      <p>🗺️ Ile de Nantes - Halle 6 Ouest &ndash; <a href="https://maps.app.goo.gl/p4f916513Q7e9W3w6">Google Maps</a>
      </p>
    </td>
  </tr>

  <tr>
    <td>
      <p style="font-weight:bold;">Visite de l'IRT Jules Verne</p>
      <p>Sur Inscription</p>
    </td>
    <td>
      <p>📆 20 juin à 14:00</p>
      <p>🗺️ 20 juin - Bouguenais - IRT Jules Verne &ndash; <a href="https://maps.app.goo.gl/zbdrfiPeFbrEZo9X8"
          target="_blank">Google Maps</a>
      </p>
    </td>
  </tr>
</table>

<br><br>

## Les activités optionnelles

* La visite de l'IRT Jules Verne est une option gratuite qui nécessite une inscription
* La soirée Networking et les Workshops sont des options payantes et nécessitent une inscription