#!/bin/bash

docker build -t "rosconfr" .

# docker run -v `pwd`:/tmp/jekyll -w /tmp/jekyll -i -t --rm --net=host rosconfr
docker run \
    -v `pwd`:/tmp/jekyll \
    -w /tmp/jekyll \
    -i -t \
    -p 0.0.0.0:4000:4000 \
    -p 0.0.0.0:35729:35729 \
    rosconfr
